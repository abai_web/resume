$(document).ready(function(){
	$('.commercial').slick({
		dots: false,
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 860,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 645,
				settings: {
		    		slidesToShow: 1,
		    		slidesToScroll: 1
		    	}
			}
		]
	});

	
	$(window).scroll(function(){
		console.clear();
		var clientHeight = document.documentElement.clientHeight;
		var scrolled = window.pageYOffset || document.documentElement.scrollTop;
		console.log('высота окна браузера: '+clientHeight);
		console.log('отскролена: '+ scrolled);

		var targetArr = $('.js-motion-bg');
		for (var i = 0; i < targetArr.length; i++) {
			var target_top = targetArr[i].getBoundingClientRect().top;
			var target_bottom = targetArr[i].getBoundingClientRect().bottom;
			console.log('конец блока позиция: '+Math.round(target_bottom));
			if (target_top <= (clientHeight/2) || Math.round(target_bottom) == clientHeight) {
				$(targetArr[i]).css({"background": "rgba(0,0,0,0.8)"});
			}
			if (target_bottom <= (clientHeight/2) || target_top >= (clientHeight/2) && Math.round(target_bottom) !==clientHeight) {
				$(targetArr[i]).css({"background": ""});
			}
			console.log("прошли: "+scrolled);
		}
	});

});
